import {StyleSheet} from 'react-native'
import {Colors} from '@common'

export default StyleSheet.create({
  input:{
    padding:0,
    fontSize:16,
    height:50,
  }
})
