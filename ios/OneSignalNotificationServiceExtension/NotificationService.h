//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by ming zhui lu on 11/21/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
